package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

class threadControllerTests {
    private threadController controller;
    private final String testSlug = "test-slug";
    private final String testAuthor = "test author";
    private final Timestamp timestamp = Timestamp.from(Instant.now());
    private final String testForum = "test forum";
    private final String testMessage = "test message";
    private final User testUser = new User(testAuthor, "test@example.com", "Test Fullname", "");
    private Thread testThread;
    private final Integer testThreadId = 0;
    private final Post testPost = new Post(testAuthor, timestamp, testForum, testMessage, 0, 0, false);
    private final List<Post> testPosts = List.of(testPost);
    private Vote testVote = new Vote(testAuthor, 1);


    @BeforeEach
    @DisplayName("Thread controller test")
    void threadTest() {
        controller = new threadController();
        testThread = new Thread(testAuthor, timestamp, testForum, testMessage, testSlug, "Thread title", 0);
        testThread.setId(testThreadId);
    }

    @Test
    @DisplayName("Correct post creation test")
    void correctlyCreatePost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.createPosts(Mockito.any(), Mockito.any(), Mockito.any()))
                    .thenAnswer((Answer<Void>) invocation -> null);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadById(Mockito.any()))
                    .thenReturn(testThread);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(testThread);

            try (MockedStatic<UserDAO> usedDaoMock = Mockito.mockStatic(UserDAO.class)) {
                usedDaoMock
                        .when(() -> UserDAO.Info(Mockito.any()))
                        .thenReturn(testUser);

                controller.createPost(testSlug, testPosts);

                threadDaoMock.verify(() -> ThreadDAO.createPosts(testThread, testPosts, List.of(testUser)));
            }
        }
    }

    @Test
    @DisplayName("Correct post get test")
    void correctlyReturnPost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.getPosts(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                    .thenAnswer((Answer<List<Post>>) invocation -> List.of(testPost));
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadById(Mockito.any()))
                    .thenReturn(testThread);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(testThread);
            Integer limit = 100;
            Integer since = null;
            String sort = null;
            Boolean desc = false;

            controller.Posts(testSlug, limit, since, sort, desc);

            threadDaoMock.verify(() -> ThreadDAO.getPosts(testThreadId, limit, since, sort, desc));
        }
    }

    @Test
    @DisplayName("Correct change post test")
    void correctlyChangePost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.change(Mockito.any(Thread.class), Mockito.any(Thread.class)))
                    .thenAnswer((Answer<Void>) invocation -> null);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadById(Mockito.any()))
                    .thenReturn(testThread);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(testThread);
            try (MockedStatic<UserDAO> usedDaoMock = Mockito.mockStatic(UserDAO.class)) {
                usedDaoMock
                        .when(() -> UserDAO.Info(Mockito.any()))
                        .thenReturn(testUser);
                controller.change(testSlug, testThread);

                threadDaoMock.verify(() -> ThreadDAO.change(testThread, testThread));
            }
        }
    }

    @Test
    @DisplayName("Correct get thread info test")
    void correctlyGetThreadInfo() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.change(Mockito.any(Thread.class), Mockito.any(Thread.class)))
                    .thenAnswer((Answer<Void>) invocation -> null);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadById(Mockito.any()))
                    .thenReturn(testThread);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(testThread);
            controller.info(testSlug);

            threadDaoMock.verify(() -> ThreadDAO.getThreadBySlug(testSlug));
        }
    }

    @Test
    @DisplayName("Correct create vote test")
    void correctlyCreateVote() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.change(Mockito.any(Thread.class), Mockito.any(Thread.class)))
                    .thenAnswer((Answer<Void>) invocation -> null);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadById(Mockito.any()))
                    .thenReturn(testThread);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(testThread);
            try (MockedStatic<UserDAO> usedDaoMock = Mockito.mockStatic(UserDAO.class)) {
                usedDaoMock
                        .when(() -> UserDAO.Info(Mockito.any()))
                        .thenReturn(testUser);
                controller.createVote(testSlug, testVote);

                threadDaoMock.verify(() -> ThreadDAO.createVote(testThread, testVote));
            }
        }
    }
}
